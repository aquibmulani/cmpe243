#include "unity.h"
#include "steer_processor.h"
#include "Mocksteering.h"

void test_steer_processor__move_left(void) {
steer_left_Expect();
steer_processor(51,50);

steer_left_Expect();
steer_processor(70,10);

}

void test_steer_processor__move_right(void) 
{
steer_right_Expect();
steer_processor(50,51);

steer_right_Expect();
steer_processor(20,80);
}

void test_steer_processor__both_sensors_less_than_threshold(void) 
{
steer_left_Expect();		
steer_processor(40,30);

steer_right_Expect();
steer_processor(30,40);
}

void test_steer_processor__do_not_move(void) 
{
steer_processor(50,50);
steer_processor(80,70);
steer_processor(40,40);	

}

// Do not modify this test case
// Modify your implementation of steer_processor() to make it pass
void test_steer_processor(void) {
  steer_right_Expect();
  steer_processor(10, 20);

  steer_left_Expect();
  steer_processor(20, 10);
}
