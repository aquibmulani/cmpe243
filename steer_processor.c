
#include "steering.h"

/**
 * Assume that a threshold value is 50cm
 * Objective is to steer left or right based on the left and right sensor values
 * Example: If left sensor is 10cm, and right is 70cm, then we should call steer_right()
 */
 

 
void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm)
{
	if(left_sensor_cm > 50 &&  right_sensor_cm <=50)
	{
		steer_left();
	}
	else if(right_sensor_cm >50 && left_sensor_cm <=50)
	{		
		steer_right();
	}	
	else if(left_sensor_cm<=50 && right_sensor_cm<=50)
	{
		if(right_sensor_cm>left_sensor_cm)
		{
			steer_right();
		}
		else if(left_sensor_cm>right_sensor_cm)
		{
			steer_left();
		}
	}
}
