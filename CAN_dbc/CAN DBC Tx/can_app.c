/*
 * can_app.c
 *
 *  Created on: 03-Mar-2019
 *      Author: Kailash Chakravarty
 */


#include <stdint.h>
#include "gpio_wrapper.h"
#include "can.h"
#include "can_app.h"



void can_app_chkBussoff(void)
{
    if (CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
    }
}

void can_app_init(void)
{
    CAN_init(can1, 100, 0, 6, 0, 0);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
}

void can_app_run(void)
{
    can_msg_t msge = {0};
    DRIVER_CMD_t sender = {0};
    can_app_send(&msge, &sender);
}

void can_app_send(can_msg_t *msge_container, DRIVER_CMD_t *sender)
{
    sender->MIA_health_status = 0x01;
    sender->LIGHT_sens_val = get_lt_sens_val();
    dbc_msg_hdr_t msge_hdr = dbc_encode_DRIVER_CMD(msge_container->data.bytes, sender);

    msge_container->msg_id = msge_hdr.mid;
    msge_container->frame_fields.data_len = msge_hdr.dlc;

    CAN_tx(can1, msge_container, 0);
}
