#include "unity.h"
#include "Mockcan.h"
#include "can_app.h"
#include "Mockgpio_wrapper.h"
#include "generated_can.h"



void test_chkBussoff_tx(void)
{
       CAN_is_bus_off_ExpectAndReturn(can1,true);	
	   CAN_reset_bus_Expect(can1);
	   can_app_chkBussoff();
	   
	   CAN_is_bus_off_ExpectAndReturn(can1,false);	
	   
	   can_app_chkBussoff();
	
}


void test_can_app_init_tx(void)
{
	
	CAN_init_ExpectAndReturn(can1, 100, 0, 6, 0, 0,true);
	CAN_bypass_filter_accept_all_msgs_Expect();
	CAN_reset_bus_Expect(can1);
	can_app_init();	
}
void test_can_app_run_tx(void)
{
	can_msg_t msge_container={0};
	get_lt_sens_val_ExpectAndReturn(50);
	CAN_tx_ExpectAndReturn(can1,&msge_container, 0,true);
	CAN_tx_IgnoreArg_msg();
	can_app_run();
}


void test_can_app_send_tx(void)
{
	can_msg_t msge_container={0};
	get_lt_sens_val_ExpectAndReturn(50);
    DRIVER_CMD_t test_1={0};
	CAN_tx_ExpectAndReturn(can1,&msge_container, 0,true);
	CAN_tx_IgnoreArg_msg();
	can_app_send(&msge_container,&test_1);
 	TEST_ASSERT_EQUAL_UINT8(0x01,test_1.MIA_health_status);
    TEST_ASSERT_EQUAL_UINT8(50,test_1.LIGHT_sens_val);	 
	// can_app_send(&msge_container,&test_1);
	
}
