/*
 * gpio_wrapper.h
 *
 *  Created on: 04-Mar-2019
 *      Author: Kailash Chakravarty
 */

#ifndef GPIO_WRAPPER_H_
#define GPIO_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

uint8_t get_lt_sens_val(void);


#endif /* GPIO_WRAPPER_H_ */
#ifdef __cplusplus
}
#endif
