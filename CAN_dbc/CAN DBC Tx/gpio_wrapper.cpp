/*
 * gpio_wrapper.cpp
 *
 *  Created on: 04-Mar-2019
 *      Author: Kailash Chakravarty
 */


#include "gpio_wrapper.h"
#include "io.hpp"



uint8_t get_lt_sens_val(void)
{
    return LS.getPercentValue();
}

