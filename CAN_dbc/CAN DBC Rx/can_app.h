/*
 * can_app.h
 *
 *  Created on: 03-Mar-2019
 *      Author: Kailash Chakravarty
 */

#ifndef CAN_APP_H_
#define CAN_APP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "can.h"
#include "generated_can.h"

void can_app_chkBussoff(void);
void can_app_init(void);
void can_app_run(void);
void can_app_recieve(can_msg_t *msge_container, DRIVER_CMD_t *rec_msge);


#endif /* CAN_APP_H_ */
#ifdef __cplusplus
}
#endif
