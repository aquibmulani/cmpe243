#include "unity.h"
#include "Mockcan.h"
#include "can_app.h"
#include "Mockgpio_wrapper.h"
#include "generated_can.h"



void test_chkBussoff_rx(void)
{
       CAN_is_bus_off_ExpectAndReturn(can1,true);	
	   CAN_reset_bus_Expect(can1);
	   can_app_chkBussoff();
	   
	   CAN_is_bus_off_ExpectAndReturn(can1,false);	
	   
	   can_app_chkBussoff();
	
}


void test_can_app_init_rx(void)
{	
	CAN_init_ExpectAndReturn(can1, 100, 6, 0, 0, 0,1);
	CAN_bypass_filter_accept_all_msgs_Expect();
	CAN_reset_bus_Expect(can1);
	can_app_init();	
}
void test_can_app_run_rx(void)
{
	can_msg_t msge_container={0};
	CAN_rx_ExpectAndReturn(can1,&msge_container, 0,true);
	CAN_rx_IgnoreArg_msg();
	can_app_run();
}


void test_can_app_receive_rx(void)
{
	can_msg_t msge_container={0};
    DRIVER_CMD_t test_1={0};
    CAN_rx_ExpectAndReturn(can1,&msge_container, 0,true);
    CAN_rx_IgnoreArg_msg(); 
    display_LED_Expect(test_1.LIGHT_sens_val); 
    msge_container.msg_id =102;
	can_app_recieve(&msge_container,&test_1);
 	

	
}
