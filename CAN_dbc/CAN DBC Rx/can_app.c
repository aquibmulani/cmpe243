/*
 * can_app.c
 *
 *  Created on: 03-Mar-2019
 *      Author: Kailash Chakravarty
 */


#include <stdint.h>
#include "can.h"
#include "can_app.h"
#include "gpio_wrapper.h"
#include "uart0_min.h"


/*const uint32_t                             DRIVER_CMD__MIA_MS = 3000; // 3 Sec
const DRIVER_CMD_t                         DRIVER_CMD__MIA_MSG = {
                                                                     .MIA_health_status = 0,
                                                                     .LIGHT_sens_val = 255,
                                                                     .mia_info.is_mia = 0,
                                                                     .mia_info.mia_counter_ms = 0

                                                                  };*/

uint32_t mia_timeout = 30; // 3 sec

void can_app_chkBussoff(void)
{
    if (CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
    }
}

void can_app_init(void)
{
    CAN_init(can1, 100, 6, 0, 0, 0);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
}

void can_app_run(void)
{
    can_msg_t msge = {0};
    DRIVER_CMD_t rec_msge = {0};

    can_app_recieve(&msge, &rec_msge);
}

void can_app_recieve(can_msg_t *msge_container, DRIVER_CMD_t *rec_msge)
{

    if (CAN_rx(can1, msge_container, 0))
    {
        dbc_msg_hdr_t msg_hdr;
        msg_hdr.dlc = msge_container->frame_fields.data_len;
        msg_hdr.mid = msge_container->msg_id;

        if (msg_hdr.mid == 102)
        {
            dbc_decode_DRIVER_CMD(rec_msge, msge_container->data.bytes, &msg_hdr);
            display_LED(rec_msge->LIGHT_sens_val);
            mia_timeout = 30; // reload mia timer
        }
    }

    if (mia_timeout-- == 0)
    {
        display_LED(50); // default value
    }

}
